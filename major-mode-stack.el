;;; major-mode-stack.el --- Save major-modes for a buffer on a stack.  -*- lexical-binding: t ; eval: (view-mode 1) -*-


;; THIS FILE HAS BEEN GENERATED.


;;; Header:

;; Copyright 2018 Marco Wahl

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Maintainer: Marco Wahl
;; Created: 2018-03-03
;; Keywords: convenience, navigation
;; URL: not yet
;; Version: 0.0.0
;; Package-Requires: ((emacs "24"))

;; This file is not part of Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; See literate companion.

;; Program
;; :PROPERTIES:
;; :ID:       5597d5a2-1048-4543-9b61-f02203b1ea02
;; :END:


;; [[id:5597d5a2-1048-4543-9b61-f02203b1ea02][Program:1]]

;;; Code:


;; Functions from other packages

;; (declare-function eshell/pwd "em-dirs.el")
;; Program:1 ends here

;; [[id:5597d5a2-1048-4543-9b61-f02203b1ea02][Program:2]]

;; variables for customization.
(defcustom major-mode-stack-modes
  '(artist-mode emacs-lisp-mode fundamental-mode org-mode text-mode)
  "Modes to change to via major-mode-stack-push.")

;; variables to be changed by the program.
(defvar-local major-mode-stack nil)
(put 'major-mode-stack 'permanent-local t) ; to not loose varibles value at mode change.
;; Program:2 ends here

;; [[id:5597d5a2-1048-4543-9b61-f02203b1ea02][Program:3]]
;;;###autoload
(defun major-mode-stack-push (maj-mode)
  "Ask for a major-mode.  If successful push recent major-mode on
a stack."
  (interactive
   (list (intern (completing-read
                  "mode: "
                  major-mode-stack-modes))))
  (setq major-mode-stack (cons major-mode major-mode-stack))
  (funcall maj-mode))
;; Program:3 ends here

;; [[id:5597d5a2-1048-4543-9b61-f02203b1ea02][Program:4]]
;;;###autoload
(defun major-mode-stack-pop ()
  "Set major mode given on the stack."
  (interactive)
  (let ((car-mm-stack  (car major-mode-stack)))
    (if (not car-mm-stack)
        (error "Nothing done!  Attempt to pop from empty stack.")
      (funcall (car major-mode-stack))
      (setq major-mode-stack (cdr major-mode-stack)))))
;; Program:4 ends here

;; Last lines
;; :PROPERTIES:
;; :ID:       f6c6a3d0-d92b-4f32-8bcf-dffe664df66a
;; :END:


;; [[id:f6c6a3d0-d92b-4f32-8bcf-dffe664df66a][Last lines:1]]

(provide 'major-mode-stack)


;;; major-mode-stack.el ends here
;; Last lines:1 ends here
